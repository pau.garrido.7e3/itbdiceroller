package com.itb.diceroller;

import androidx.appcompat.app.AppCompatActivity;

import android.annotation.SuppressLint;
import android.os.Bundle;
import android.view.Gravity;
import android.view.MotionEvent;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import java.util.Random;

public class MainActivity extends AppCompatActivity {

    ImageView dice1;
    ImageView dice2;
    Button rollButton, returnButton;


    @SuppressLint("ClickableViewAccessibility")
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);


        dice1 = findViewById(R.id.imageView);
        rollButton = findViewById(R.id.rollButton);
        returnButton = findViewById(R.id.returnButton);
        dice2 = findViewById(R.id.imageView1);


        returnButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                rollButton.setText("Roll the dice");
                dice1.setImageResource(R.drawable.empty_dice);
                dice2.setImageResource(R.drawable.empty_dice);
            }
        });

        dice1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                int[] images = {R.drawable.dice_1, R.drawable.dice_2, R.drawable.dice_3, R.drawable.dice_4, R.drawable.dice_5, R.drawable.dice_6};
                Random rand = new Random();
                int numeroDice1;
                numeroDice1 = rand.nextInt(6);
                dice1.setImageResource(images[numeroDice1]);
            }
        });

        dice2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                int[] images = {R.drawable.dice_1, R.drawable.dice_2, R.drawable.dice_3, R.drawable.dice_4, R.drawable.dice_5, R.drawable.dice_6};
                Random rand = new Random();
                int numeroDice2;
                numeroDice2 = rand.nextInt(6);
                dice2.setImageResource(images[numeroDice2]);
            }
        });

        rollButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                rollButton.setText("Dice Rolled");
                int[] images = {R.drawable.dice_1, R.drawable.dice_2, R.drawable.dice_3, R.drawable.dice_4, R.drawable.dice_5, R.drawable.dice_6};
                Random rand = new Random();
                int numeroDice1, numeroDice2;
                numeroDice1 = rand.nextInt(6);
                numeroDice2 = rand.nextInt(6);
                dice1.setImageResource(images[numeroDice1]);
                dice2.setImageResource(images[numeroDice2]);

                if (numeroDice1 == 5 && numeroDice2 == 5){
                    Toast mytoast = Toast.makeText(getApplicationContext(), "JACKPOOOOOOOOOT!!", Toast.LENGTH_SHORT);
                    mytoast.setGravity(Gravity.TOP, 0, 0);
                    mytoast.show();

                }
            }
        });



    }
}